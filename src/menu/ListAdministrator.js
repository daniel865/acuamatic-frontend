import React,  { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import EventIcon from '@material-ui/icons/Event';
import SettingsInputComponentIcon from '@material-ui/icons/SettingsInputComponent';
import SearchIcon from '@material-ui/icons/Search';

import { Link } from 'react-router-dom'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class ListAdministrator extends Component {
    
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <List component="nav">
                    <Link to='/'>
                        <ListItem button>
                            <ListItemIcon>
                                <SearchIcon />
                            </ListItemIcon>
                            <ListItemText primary="Monitor" />
                        </ListItem>
                    </Link>
                </List>
                <Divider />
                <List component="nav">
                    <Link to='/connections'>
                        <ListItem button>
                            <ListItemIcon>
                                <SettingsInputComponentIcon />
                            </ListItemIcon>
                            <ListItemText primary="Historial de Conexiones" />
                        </ListItem>
                    </Link>
                </List>
                <List component="nav">
                    <Link to='/events'>
                        <ListItem button>
                            <ListItemIcon>
                                <EventIcon />
                            </ListItemIcon>
                            <ListItemText primary="Historial Eventos" />
                        </ListItem>
                    </Link>
                </List>
            </div>
        );
    }

}


ListAdministrator.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListAdministrator);