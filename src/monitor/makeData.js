const data = [
  {
    "id": "3e548519-3000-44ee-b5f5-7835d2755dd1",
    "name": "Bucaros",
    "devices": [
      {
        "id": "29509dc0-ed50-4bb5-8672-fd1b956cc8f9",
        "name": "ESP-B1",
        "equipments": [
          {
            "id": "44c81812-4c52-437a-9e52-82ac85b2516f",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "cde126ab-74cb-4c43-a0de-c6320277ea65",
                "name": "ON/OFF"
              },
              {
                "id": "3dc0a621-096e-45af-b35b-5e232870c442",
                "name": "Presion"
              }
            ]
          },
          {
            "id": "eb11ba41-0874-41cf-a08c-0126a8e4946d",
            "name": "Tanque",
            "parameters": [
              {
                "id": "29bc5e68-3ad7-41b1-8254-c608681a578d",
                "name": "Nivel"
              },
              {
                "id": "b328d6f6-2ace-4db8-b497-892a2b6a2f71",
                "name": "ON/OFF"
              }
            ]
          }
        ]
      },
      {
        "id": "2a074d6e-cedf-477d-86fd-900d2f8f338c",
        "name": "ESP-B2",
        "equipments": [
          {
            "id": "d068cddf-5b62-45cb-af77-1a1719479439",
            "name": "Tanque",
            "parameters": [
              {
                "id": "af11cc1d-e690-462f-8399-717350ad4e3e",
                "name": "ON/OFF"
              },
              {
                "id": "d18b13ac-4b67-455f-b8ea-736d1bb09d0d",
                "name": "Nivel"
              }
            ]
          },
          {
            "id": "28ca3480-24e7-4486-8474-f0103197c22b",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "da940d9f-a5b7-4470-8abf-220fb000db1e",
                "name": "Temperatura"
              },
              {
                "id": "1321cf06-1abf-413d-b619-49fb73c8f075",
                "name": "ON/OFF"
              },
              {
                "id": "c05457c3-33cd-4d9b-be53-22ede834bd93",
                "name": "Presion"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    "id": "686c512e-cdab-491d-a170-e13dbac428ff",
    "name": "Alcazares",
    "devices": [
      {
        "id": "4f15685c-415b-44f2-b035-af2de4937332",
        "name": "ESP-A1",
        "equipments": [
          {
            "id": "95522c7d-cf61-43e6-b0a2-5d3981ead106",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "bbb1f206-955b-4b16-9178-5572253a82af",
                "name": "ON/OFF"
              },
              {
                "id": "9cf3536e-ae89-4d7d-8eac-43b1d5889f1e",
                "name": "Presion"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    "id": "a9f42209-4291-4377-93bd-22196e2ae111",
    "name": "Castellanes",
    "devices": [
      {
        "id": "c5b8c77a-9014-4716-b5e5-e88d3c9fee62",
        "name": "ESP-C1",
        "equipments": [
          {
            "id": "9fb3414b-cb29-4a94-80b4-a97a04fdcd6a",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "2e18dd56-f676-4b56-97d9-6aa3d8aceafc",
                "name": "ON/OFF"
              }
            ]
          }
        ]
      },
      {
        "id": "29d4c777-880a-4164-825d-68779050361c",
        "name": "ESP-C2",
        "equipments": [
          {
            "id": "59112b8e-eb00-4c3a-8c7e-a742eaea6c16",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "a38e4601-2e56-4390-954b-e4274a548485",
                "name": "ON/OFF"
              }
            ]
          }
        ]
      },
      {
        "id": "f6de02f5-d9d8-4501-b94c-7fff85cec715",
        "name": "ESP-C3",
        "equipments": [
          {
            "id": "ca4be426-4862-4fb8-8430-482e908cdef3",
            "name": "Bomba de Agua",
            "parameters": [
              {
                "id": "8bbd1536-6016-4eb2-852e-95783e76b516",
                "name": "ON/OFF"
              }
            ]
          }
        ]
      }
    ]
  }
]

export default data;