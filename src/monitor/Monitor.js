import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import data from './makeData';
import NodeItem from './node/NodeItem';
import ModalItem from './modal/ModalItem';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  nested: {
    listStyle: 'none',
    margin: 0,
    padding: '5px 20px',
    width: '100%'
  },
  ul: {
    li: {
      padding: '5px',
    }
  },
  ul: {
    li: {
      ul: {
        listStyle: 'none',
        margin: 0,
        padding: '0 20px',
      }
    }
  },
  ul: {
    li: {
      ul: {
        li: {
          '&:hover': {
            color: '#fff',
            cursor: 'pointer',
            backgroundColor: '#00BCD1',
            borderRadious: '5px',
          }
        }
      }
    }
  }
});

class Monitor extends React.Component {
  
  state = {
    openModal: false,
  };
 
  handleOpenModal = () => {
    this.setState({ openModal: true });
  };

  handleCloseModal = () => {
    this.setState({ openModal: false });
  };

  getStations = stations => {
    return stations.map(station => (
      <ul>
        <NodeItem itemName="Estación" item={station} handleOpenModal={this.handleOpenModal} />
        { this.getDevicesByStation(station) }
      </ul>
    ));
  };

  getDevicesByStation = station => {
    return (
      station.devices.map(device => (
        <ul>
          <NodeItem itemName="Dispositivo" item={device} handleOpenModal={this.handleOpenModal} />
          { this.getEquipmentsByDevice(device) }
        </ul>
      ))
    );
  };

  getEquipmentsByDevice = device => {
    return (
      device.equipments.map(equipment => (
        <ul>
          <NodeItem itemName="Equipo" item={equipment} handleOpenModal={this.handleOpenModal} />
          { this.getParamsByEquipment(equipment) }
        </ul>
      ))
    );
  };

  getParamsByEquipment = equipment => {
    return (
      <ul>
        {
          equipment.parameters.map(param => (
            <NodeItem itemName="Parámetro" item={param} handleOpenModal={this.handleOpenModal} />
          ))  
        }
      </ul>
    )
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container className={classes.root}>
        <Grid item justify="left" xs={12}>
          <ul className={classes.nested}>
            { this.getStations(data) }
          </ul>
        </Grid>

        <ModalItem openModal={this.state.openModal} handleCloseModal={this.handleCloseModal} />
      </Grid>
    );
  }
}

Monitor.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Monitor);