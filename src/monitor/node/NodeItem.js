import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    node: {
        width: '100%',
        display: 'block',
        minWidth: '50px',
        minHeight: '50px',
    },
    buttonContainer: {
        display: 'inline-block',
    },
    messageContainer: {
        display: 'inline-block',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class NodeItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isHovering: false,
        };
    };

    handleMouseHover = () => {
        this.setState(this.toggleHoverState);
    };
    
    toggleHoverState(state) {
        return {
          isHovering: !state.isHovering,
        };
    }

    render() {
        const { classes } = this.props;
        const { item } = this.props;

        return (
            <Grid item 
                className={classes.node}
                onMouseEnter={this.handleMouseHover}
                onMouseLeave={this.handleMouseHover}
            >
                <div className={classes.messageContainer}>
                    { item.name }
                </div>
                <div className={classes.buttonContainer}>
                    { this.state.isHovering && 
                        <Button variant="contained" className={classes.button} onClick={this.props.handleOpenModal}>
                            Añadir { this.props.itemName }
                        </Button> 
                    }
                </div>
            </Grid>
        )
    }
    
}

NodeItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NodeItem);