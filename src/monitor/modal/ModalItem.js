import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Modal, Button } from '@material-ui/core';

function getModalStyle() {
    const top = 50;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    }
  }
  
  const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
    },
  });

class ModalItem extends React.Component {

    state = {
        openModal: false,
    }

    handleOpen = () => {
        this.setState({ openModal: true });
    };

    handleClose = () => {
        this.setState({ openModal: false });
    };

    handleCreateItem = () => {
        alert('Creating Item');
    };

    render() {
        const { classes } = this.props;

        return (
            <Grid container>
                <Modal 
                    aria-labelledby="Crear"
                    aria-describedby="Crear Item"
                    open={this.props.openModal}
                    onClose={this.handleClose}
                >
                    <div style={ getModalStyle() } className={classes.paper}>

                        <h1>Form</h1>

                        <Button variant="text" color="secondary" onClick={this.props.handleCreateItem}>
                            Crear
                        </Button>
                        <Button variant="text" color="primary" onClick={this.props.handleCloseModal}>
                            Cerrar
                        </Button>
                    </div>
                </Modal>
            </Grid>
        )
    }

}

export default withStyles(styles)(ModalItem);