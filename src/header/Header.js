import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';

import ListAdministrator from '../menu/ListAdministrator';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    list:  {
        width: 250,
    },
    fullList: {
        width: 'auto'
    }
};

class Header extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    toggleDrawer = () => this.setState({ open: !this.state.open })

    render() {
        const { classes } = this.props;

        return (
            <header>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Toolbar>
                        <IconButton 
                            className={classes.menuButton} 
                            color="inherit" 
                            aria-label="Menu"
                            onClick={this.toggleDrawer}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            Acuamatic
                        </Typography>
                        <Button color="inherit">Login</Button>
                        </Toolbar>
                    </AppBar>

                    <Drawer
                        open={this.state.open}
                        onClose={this.toggleDrawer}
                        docked={false}
                        width={300}
                    >
                        <div
                            className={classes.list}
                            tabIndex={0}
                            role="button"
                            onClick={this.toggleDrawer}
                            onKeyDown={this.toggleDrawer}
                        >
                            <ListAdministrator />
                        </div>
                    </Drawer>
                </div>
            </header>
        )
    }

}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header); 
