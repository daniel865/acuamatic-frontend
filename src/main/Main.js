import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Monitor from '../monitor/Monitor';
import ConnectionsHistory from '../connectionsHistory/ConnectionsHistory';
import EventHistory from '../eventHistory/EventHistory';

const Main = () => (
    <main>
        <Switch>
            <Route exact path="/" component={ Monitor } />
            <Route path="/connections" component={ ConnectionsHistory } />
            <Route path="/events" component={ EventHistory } />
        </Switch>
    </main>
)

export default Main;