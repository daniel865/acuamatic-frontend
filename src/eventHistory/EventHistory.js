import React from 'react';
import { Grid, TextField } from '@material-ui/core';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';
import TableEvents from './TableEvents';
import ModalParams from '../utils/modalParams/ModalParams';

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing.unit,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    button: {
        margin: 1,
    },
});

class EventHistory extends React.Component {

    state = {
        station: '',
        initDate: '',
        endDate: '',
        openModal: false,
    };

    handleOpenModal = () => {
        this.setState({ openModal: true })
    }

    handleCloseModal = () => {
        this.setState({ openModal: false })
    }
    
    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Grid container className={classes.root} >
                    <Grid 
                        container
                        item 
                        xs={12}
                        direction="column"
                        alignItems="center"
                        justify="center"
                    >

                        <Typography variant="h6" gutterBottom>
                            Historial de Eventos
                        </Typography>

                        <form className={classes.container} noValidate>
                            <TextField 
                                id="station"
                                label="Estación"
                                className={classes.textField}
                                value={this.state.station}
                                onChange={this.handleChange('station')}
                                margin="normal"
                            />

                            <TextField
                                id="initDate"
                                label="Fecha de Inicio"
                                type="datetime-local"
                                className={classes.textField}
                                value={this.state.initDate}
                                onChange={this.handleChange('initDate')}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                margin="normal"
                            />

                            <TextField 
                                id="endDate"
                                label="Fecha de Fin"
                                type="datetime-local"
                                className={classes.textField}
                                value={this.state.endDate}
                                onChange={this.handleChange('endDate')}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                margin="normal"
                            />

                            <Button variant="text" color="primary" className={classes.button} onClick={this.handleOpenModal}>
                                Añadir Parametro
                            </Button>

                            <TableEvents  />
                        </form>
                    </Grid>
                </Grid>


                <ModalParams openModal={this.state.openModal} handleCloseModal={this.handleCloseModal} />
            </div>
        );
    }

}

export default withStyles(styles)(EventHistory);