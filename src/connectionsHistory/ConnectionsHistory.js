import React, { Component } from 'react';

import { Grid, TextField } from '@material-ui/core';

import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';
import TableConnections from './TableConnections';

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing.unit,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    button: {
        margin: 1,
    },
});

class ConnectionsHistory extends Component {

    state = {
        station: '',
        initDate: '',
        endDate: '',
    };
    
    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Grid container className={classes.root} >
                    <Grid 
                        container
                        item 
                        xs={12}
                        direction="column"
                        alignItems="center"
                        justify="center"
                    >

                        <Typography variant="h6" gutterBottom>
                            Historial de Conexiones
                        </Typography>

                        <form className={classes.container} noValidate>
                            <TextField 
                                id="station"
                                label="Estación"
                                className={classes.textField}
                                value={this.state.station}
                                onChange={this.handleChange('station')}
                                margin="normal"
                            />

                            <TextField
                                id="initDate"
                                label="Fecha de Inicio"
                                type="datetime-local"
                                className={classes.textField}
                                value={this.state.initDate}
                                onChange={this.handleChange('initDate')}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                margin="normal"
                            />

                            <TextField 
                                id="endDate"
                                label="Fecha de Fin"
                                type="datetime-local"
                                className={classes.textField}
                                value={this.state.endDate}
                                onChange={this.handleChange('endDate')}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                margin="normal"
                            />

                            <TableConnections />
                        </form>
                    </Grid>
                </Grid>
            </div>
        );
    }

} 

export default withStyles(styles)(ConnectionsHistory);