import React, { Component } from 'react';
import './App.css';

import { BrowserRouter } from 'react-router-dom'
import Header from './header/Header';
import Main from './main/Main';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Main />
      </div>
    );
  }
}

export default () => (
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
