const data = {
  "id": "b4012cb7-acac-4b33-a9a8-7219ca8af7f4",
  "station": "Castellanes",
  "devices": [
    {
      "id": "15af2007-a986-4683-9174-9629f54e454d",
      "name": "ESP-C1",
      "equipments": [
        {
          "id": "c0a6d18f-82e3-41bd-a6f6-55ed11027a6e",
          "name": "Bomba",
          "params": [
            {
              "id": "e3c9ef5d-982a-4c1c-8572-b7a66c8f3de7",
              "name": "Nivel Agua"
            },
            {
              "id": "daeec424-519c-42e6-92a6-56049a77dde0",
              "name": "Presion"
            },
            {
              "id": "b8dbace8-29dc-4740-a808-e5bfc4c3a1fb",
              "name": "ON/OFF"
            }
          ]
        },
        {
          "id": "e29e0ea6-a46e-4d63-b014-aa18921c6bd3",
          "name": "Tanque",
          "params": [
            {
              "id": "62a137ab-c439-4263-95e5-e50ac6ff68b1",
              "name": "Temperatura"
            },
            {
              "id": "dd80bc1d-381b-4f7f-b639-1597b4b84327",
              "name": "ON/OFF"
            }
          ]
        }
      ]
    },
    {
      "id": "d1cbef88-4ae6-43ac-8b2e-1f95515b8631",
      "name": "ESP-B2",
      "equipments": [
        {
          "id": "ee1f00e1-49f7-47ff-9532-8ee3ed0149bb",
          "name": "Bomba",
          "params": [
            {
              "id": "1d172fa6-7a5c-4895-b3ec-3641a26ad09f",
              "name": "Nivel Agua"
            },
            {
              "id": "1ef01630-da2a-4fe8-91bb-1d40c269f19b",
              "name": "Presion"
            }
          ]
        },
        {
          "id": "0f2dd5c1-9195-4290-90db-adf84d3d08a4",
          "name": "Tanque",
          "params": [
            {
              "id": "b28d6c10-e386-482b-b55c-aa3d61e2a682",
              "name": "Temperatura"
            },
            {
              "id": "def059cd-3f3a-4c2c-84bf-feec8f6988c2",
              "name": "ON/OFF"
            }
          ]
        }
      ]
    }
  ]
}

export default data