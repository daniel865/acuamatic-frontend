import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';

class ListParams extends React.Component {

    state = {
        checkedItems: new Map()
    }
    
    handleChange = event => {
        const item = event.target.name;
        const isChecked = event.target.checked;

        this.setState(prevState => ({
            checkedItems: prevState.checkedItems.set(item, isChecked)
        }))
    }

    getEquipmentsFromDevice = device => {
        return (
            <React.Fragment>
                {
                    device.equipments.map(equipment => (
                        this.getParamsInEquipment(equipment) 
                    )) 
                }
            </React.Fragment>
        )
    }

    getParamsInEquipment = equipment => {
        return (
            <ul>
                <li>{equipment.name}</li>
                <ul>
                    {
                        equipment.params.map(param => (
                            <li>
                                <label>
                                    {param.name}
                                    <Checkbox
                                        checked={this.state.checkedItems.get(param.id)}
                                        onChange={this.handleChange}
                                        value="checkedA"
                                    />
                                </label>
                            </li>
                        ))
                    }
                </ul>    
            </ul>    
        )
    }

    render() {
        const { station } = this.props; 

        return (
            <div>
                { 
                    station.devices.map(device => this.getEquipmentsFromDevice(device)) 
                }
            </div>
        )
    }
}

export default ListParams;  