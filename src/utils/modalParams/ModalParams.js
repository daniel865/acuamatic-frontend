import React from 'react';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import ListParams from '../listParams/ListParams';
import { Grid, Button } from '@material-ui/core';

import data from '../listParams/makeData';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  }
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
});

class ModalParams extends React.Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Grid container>
          <Modal
            aria-labelledby="Dispositivos por parametros"
            aria-describedby="Seleccione los parametros"
            open={this.props.openModal}
            onClose={this.handleClose}
          >
            <div style={ getModalStyle() } className={classes.paper}>
              <ListParams station={data} />

              <Button variant="text" color="primary" onClick={this.props.handleCloseModal}>
                Cerrar
              </Button>
            </div>
          </Modal>
        </Grid>
      </div>
    );
  }
}

ModalParams.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ModalParams);